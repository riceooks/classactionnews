package tw.edu.hust.yu97024.view;

import java.util.ArrayList;
import java.util.List;

import tw.edu.hust.yn97024.ApplocationBody;
import tw.edu.hust.yn97024.R;
import tw.edu.hust.yn97024.SettingActivity;
import tw.edu.hust.yn97024.list.ListCategory;
import tw.edu.hust.yn97024.packge.CategoryList;
import tw.edu.hust.yn97024.packge.PostList;
import tw.edu.hust.yn97024.skill.SQLite;
import tw.edu.hust.yn97024.skill.Soap;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ViewList extends Activity implements OnClickListener,
		OnItemClickListener, OnItemLongClickListener, Runnable, OnTouchListener {
	private static final String TAG = "ViewList";
	// -- 宣告區 --//
	// ~ ~ ~ ~ ~ 元件
	private ImageView ic_logo;
	private ListView list_category;
	private RelativeLayout btn_one, btn_two, btn_three;
	private TextView text_one, text_two, text_three;
	private ProgressDialog progress;

	// ~ ~ ~ ~ ~
	private Soap soap = new Soap();
	// 目錄資料
	private List<CategoryList> categoryList = new ArrayList<CategoryList>();
	// 文章資料
	private List<PostList> postList = new ArrayList<PostList>();
	private ListCategory listCtegory;
	private Intent intent = null;

	// ~ ~ ~ ~ ~ 其他
	// 要查詢的目錄編號
	private int catID = 0;
	private int list_id;
	// 查詢目錄時
	private final static int QUERY_CATEGORY = 1;
	private final static int QUERY_POSTS = 2;
	private final static int CLOSE_DAILOG = 3;
	private final int QUERY_NO_NETWORK = 4;
	private static final int QUERY_EMPTY_BACK = 0;
	private SharedPreferences mPerferences;
	private SQLite sqlite;
	@SuppressWarnings("unused")
	private boolean moveStatus = false;
	private ApplocationBody applocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_category);

		// 偏好設定
		mPerferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		applocation = (ApplocationBody) getApplication();
		
		// SQLite
		if (sqlite == null) {
			sqlite = applocation.bodySQLite;
		}

		// -- 元件產生區 --//
		// 標誌
		ic_logo = (ImageView) findViewById(R.id.ic_logo);
		// 清單列表
		list_category = (ListView) findViewById(R.id.list_category);
		listCtegory = new ListCategory(this);
		// 下方功能表1
		btn_one = (RelativeLayout) findViewById(R.id.btn_one);
		text_one = (TextView) findViewById(R.id.text_one);
		// 下方功能表2
		btn_two = (RelativeLayout) findViewById(R.id.btn_two);
		text_two = (TextView) findViewById(R.id.text_two);
		// 下方功能表3
		btn_three = (RelativeLayout) findViewById(R.id.btn_three);
		text_three = (TextView) findViewById(R.id.text_three);

		// 接收區
		intent = getIntent();

		list_id = intent.getIntExtra("list_id", -1);
		List<CategoryList> regCategoryList;
		if (list_id > -1) {
			Log.i("Get Intent Type", "目錄");

			// Logo 回首頁
			ic_logo.setTag("home");
			ic_logo.setOnClickListener(this);

			// 取得目錄編號
			regCategoryList = intent
					.getParcelableArrayListExtra("categoryList");
			catID = Integer
					.valueOf(regCategoryList.get(list_id).term_taxonomy_id);

			// 下階層的按鈕
			setTitle(regCategoryList.get(list_id).name);
			btn_one.setTag("back");
			text_one.setText("回上一頁");
		} else {

			Log.e("Get Intent", "Switch To Default");
			setTitle("首頁");

			// 第一階層的按鈕
			btn_one.setTag("exit");
			text_one.setText("離開程式");
		}

		btn_one.setOnClickListener(this);
		btn_two.setOnClickListener(this);
		btn_three.setOnClickListener(this);
		btn_one.setOnTouchListener(this);
		btn_two.setOnTouchListener(this);
		btn_three.setOnTouchListener(this);
		btn_two.setTag("setting");
		text_two.setText("設定");
		btn_three.setTag("about");
		text_three.setText("關於");

		// -- 取資料 -- //
		// 顯示載入視窗
		progress = ProgressDialog.show(ViewList.this, "提示訊息", "資料載入中...",
				false, false);

		new Thread(this).start();
	}

	@Override
	public void run() {
		// 存放SOAP查回的資料
		String[][] soapQueryCategory = null;
		String[][] soapQueryPosts = null;
		try {
			// 網路瀏覽資料
			if (mPerferences.getBoolean(applocation.KEY_USE_INTERNET_SQL_DATA, true)) {
				if(applocation.isNetwork()){
					soapQueryCategory = soap.GetCategory(String.valueOf(catID));
					soapQueryPosts = soap.GetPostList(String.valueOf(catID));
				} else {
					handler.obtainMessage(QUERY_NO_NETWORK, "您沒有開啟網路連線，請使用離線查看資料。").sendToTarget();
				}
			}
			// 本地瀏覽資料
			else {
				
				String script = "";
				script = "SELECT term_taxonomy_id, term_id, name, taxonomy, parent, count, cat_count "
						+ " FROM category_list "
						+ " WHERE parent = "
						+ String.valueOf(catID) + " ORDER BY term_id ";
				soapQueryCategory = sqlite.SQLQuery(script);
				script = "SELECT id, post_author, post_date, post_title, comment_count, cat_id "
						+ " FROM posts_list "
						+ " WHERE cat_id = "
						+ String.valueOf(catID) + " ORDER BY id ";
				soapQueryPosts = sqlite.SQLQuery(script);
				
				if((soapQueryCategory == null || soapQueryCategory.length == 0) && (soapQueryPosts == null || soapQueryPosts.length == 0)){
					Log.d(TAG, "SOAP return");
					handler.obtainMessage(QUERY_EMPTY_BACK, "還沒有取得目錄中的資料，請進入設定進行更新或使用網路瀏覽。").sendToTarget();
				}
				
				// */
			}
		} catch (Exception e) {
			Log.e(TAG, "SOAP get category error!!", e);
		} finally {
			progress.dismiss();
			// 目錄列表資料
			if (soapQueryCategory != null && soapQueryCategory.length > 0) {
				handler.obtainMessage(QUERY_CATEGORY, soapQueryCategory)
						.sendToTarget();
			}
			// 文章列表資料
			if (soapQueryPosts != null && soapQueryPosts.length > 0) {
				handler.obtainMessage(QUERY_POSTS, soapQueryPosts)
						.sendToTarget();
			}

			handler.sendEmptyMessage(CLOSE_DAILOG);
		}
	}

	public Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			String[][] data = null;
			switch (msg.what) {
			// 資料是空的
			case QUERY_EMPTY_BACK:
				setResult(RESULT_OK);
				new AlertDialog.Builder(ViewList.this)
				.setTitle("提示訊息")
				.setMessage((String)msg.obj)
				.setPositiveButton("確定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						ViewList.this.finish();
					}
				})
				.setNegativeButton("設定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						intent = new Intent(ViewList.this, SettingActivity.class);
						startActivityForResult(intent, RESULT_FIRST_USER);
					}
				})
				.show();
				break;
			// 目錄
			case QUERY_CATEGORY:
				data = (String[][]) msg.obj;

				for (String[] sa : data) {
					categoryList.add(new CategoryList(sa));
				}

				listCtegory.clear();

				for (int i = 0; i < data.length; i++) {
					listCtegory.AppendItem(R.drawable.ic_category, categoryList
							.get(i).name,
							getResources().getDrawable(R.drawable.arrow), i);

					// 插入SQLite
					if (mPerferences.getBoolean("useInternetSQLData", false)) {
						String queryCount = "0";
						queryCount = sqlite
								.SQLQueryOne("SELECT COUNT(*) FROM category_list WHERE term_taxonomy_id = "
										+ categoryList.get(i).term_taxonomy_id);
						// SQLite有資料
						String script = "";
						if (Integer.valueOf(queryCount) > 0) {
							script = "UPDATE category_list " + " SET "
									+ " term_id = '"
									+ categoryList.get(i).term_id + "', "
									+ " name = '" + categoryList.get(i).name
									+ "', " + " taxonomy = '"
									+ categoryList.get(i).taxonomy + "', "
									+ " parent = '"
									+ categoryList.get(i).parent + "', "
									+ " count = '" + categoryList.get(i).count
									+ "', " + " cat_count = '"
									+ categoryList.get(i).cat_count + "' "
									+ " WHERE term_taxonomy_id = '"
									+ categoryList.get(i).term_taxonomy_id
									+ "'; ";
							Log.d("ViewList", script);
							sqlite.SQLExecute(script);
						}
						// SQLite沒有資料
						else {
							script = "INSERT INTO category_list (term_taxonomy_id, term_id, name, taxonomy, parent, count, cat_count) "
									+ " VALUES( "
									+ " '"
									+ categoryList.get(i).term_taxonomy_id
									+ "', "
									+ " '"
									+ categoryList.get(i).term_id
									+ "', "
									+ " '"
									+ categoryList.get(i).name
									+ "', "
									+ " '"
									+ categoryList.get(i).taxonomy
									+ "', "
									+ " '"
									+ categoryList.get(i).parent
									+ "', "
									+ " '"
									+ categoryList.get(i).count
									+ "', "
									+ " '"
									+ categoryList.get(i).cat_count
									+ "' "
									+ " ); ";
							Log.d("ViewList", script);
							sqlite.SQLExecute(script);
						}
					}
				}
				list_category.setAdapter(listCtegory);

				list_category.setOnItemClickListener(ViewList.this);
				list_category.setOnItemLongClickListener(ViewList.this);

				break;
			// 文章
			case QUERY_POSTS:
				data = (String[][]) msg.obj;
				for (int i = 0; i < data.length; i++) {
					postList.add(new PostList(data[i]));
				}
				for (int i = 0; i < data.length; i++) {
					listCtegory.AppendItem(R.drawable.ic_post,
							postList.get(i).post_title, getResources()
									.getDrawable(R.drawable.arrow), i);

					// 插入SQLite
					if (mPerferences.getBoolean("useInternetSQLData", false)) {
						String queryCount = "0";
						queryCount = sqlite
								.SQLQueryOne("SELECT COUNT(*) FROM posts_list WHERE id =  "
										+ postList.get(i).id);
						// SQLite有資料
						String script = "";
						if (Integer.valueOf(queryCount) > 0) {
							script = "UPDATE posts_list " + " SET "
									+ " post_author = '"
									+ postList.get(i).post_author + "', "
									+ " post_date = '"
									+ postList.get(i).post_date + "', "
									+ " post_title = '"
									+ postList.get(i).post_title + "', "
									+ " comment_count = '"
									+ postList.get(i).comment_count + "', "
									+ " cat_id = '" + postList.get(i).cat_id
									+ "' " + " WHERE id = '"
									+ postList.get(i).id + "'; ";
							Log.d("ViewList", script);
							sqlite.SQLExecute(script);
						}
						// SQLite沒有資料
						else {
							script = "INSERT INTO posts_list (id, post_author, post_date, post_title, comment_count, cat_id)  "
									+ " VALUES( "
									+ " '"
									+ postList.get(i).id
									+ "', "
									+ " '"
									+ postList.get(i).post_author
									+ "', "
									+ " '"
									+ postList.get(i).post_date
									+ "', "
									+ " '"
									+ postList.get(i).post_title
									+ "', "
									+ " '"
									+ postList.get(i).comment_count
									+ "', "
									+ " '"
									+ postList.get(i).cat_id
									+ "' " + " ); ";
							Log.d("ViewList", script);
							sqlite.SQLExecute(script);
						}
					}
				}
				list_category.setAdapter(listCtegory);

				list_category.setOnItemClickListener(ViewList.this);
				list_category.setOnItemLongClickListener(ViewList.this);

				break;
			case QUERY_NO_NETWORK:
				new AlertDialog.Builder(ViewList.this)
				.setTitle("提示訊息")
				.setMessage((String)msg.obj)
				.setPositiveButton("確定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						setResult(RESULT_CANCELED);
						ViewList.this.finish();
					}
				})
				.setNegativeButton("設定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						intent = new Intent(ViewList.this, SettingActivity.class);
						startActivityForResult(intent, RESULT_FIRST_USER);
					}
				})
				.show();
				break;
			}
		}
	};

	@Override
	public void onBackPressed() {
		if (list_id > -1) {
			findViewById(R.id.btn_one).setTag("back");
			onClick(findViewById(R.id.btn_one));
		} else {
			String appName = getString(R.string.app_name);
			Builder ad = new AlertDialog.Builder(ViewList.this);
			ad.setTitle(appName);
			ad.setMessage(String.format("您確定要離開%s嗎?", appName));
			ad.setPositiveButton(android.R.string.no, null);
			ad.setNegativeButton(android.R.string.yes,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							setResult(RESULT_CANCELED);
							finish();
						}
					});
			ad.show();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.ic_logo:
		case R.id.btn_one:
		case R.id.btn_two:
		case R.id.btn_three:
			// 設定
			if (view.getTag().equals("setting")) {
				Log.d("Bottom Button Click", "設定");
				intent = new Intent(ViewList.this, SettingActivity.class);
				startActivityForResult(intent, RESULT_FIRST_USER);
			}
			// 回上一頁
			else if (view.getTag().equals("back")) {
				Log.d("View Click", view + "");
				setResult(RESULT_OK);
				ViewList.this.finish();
			}
			// 關於
			else if (view.getTag().equals("about")) {
				Log.d("Bottom Button Click", "關於");
				applocation.showAboutDialog(this);
			}
			// 回首頁
			else if (view.getTag().equals("home")) {
				setResult(RESULT_CANCELED);
				finish();
				intent = new Intent(this, ViewList.class);
				startActivity(intent);
			}
			// 離開程式
			else if (view.getTag().equals("exit")) {
				String appName = getString(R.string.app_name);

				Builder ad = new AlertDialog.Builder(ViewList.this);
				ad.setTitle(appName);
				ad.setMessage(String.format("您確定要離開%s嗎?", appName));
				ad.setPositiveButton(android.R.string.no, null);
				ad.setNegativeButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								setResult(RESULT_CANCELED);
								finish();
							}
						});
				ad.show();
			}
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case ApplocationBody.RESULT_SETTING :
			progress = ProgressDialog.show(this, "提示訊息", "資料載入中...",false, false);
			new Thread(this).start();
			break;
		case RESULT_CANCELED:
			setResult(RESULT_CANCELED);
			finish();
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int id, long arg3) {
		// 取得點擊類型
		int type = (Integer) view.findViewById(R.id.ic_type).getTag();
		int list_id = (Integer) view.findViewById(R.id.text_name).getTag();
		if (type == R.drawable.ic_category) {
			toNextPage(type, list_id, ViewList.class);
		} else if (type == R.drawable.ic_post) {
			toNextPage(type, list_id, ViewPost.class);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void toNextPage(final int type, final int list_id, Class<?> activity) {
		try {
			// 類型為目錄
			if (type == R.drawable.ic_category) {
				Log.i(TAG, "List type 目錄  => 編號:"
						+ categoryList.get(list_id).term_taxonomy_id);
				intent = new Intent(ViewList.this, activity);
				intent.putParcelableArrayListExtra("categoryList",
						(ArrayList) categoryList);
				intent.putExtra("list_id", list_id);
				startActivityForResult(intent, 0);
			}
			// 類型為文章
			else if (type == R.drawable.ic_post) {
				Log.i("List type", "文章");
				intent = new Intent(ViewList.this, activity);
				intent.putParcelableArrayListExtra("postList",
						(ArrayList) postList);
				intent.putExtra("list_id", list_id);
				startActivityForResult(intent, 0);
			}
		} catch (Exception e) {
			Log.e("ListView Error", "ListView Click Error => "
					+ e.getMessage().toString(), e);
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> adapter, View view, int id,
			long arg3) {
		final int type = (Integer) view.findViewById(R.id.ic_type).getTag();
		final int list_id = (Integer) view.findViewById(R.id.text_name)
				.getTag();

		Builder ad = new AlertDialog.Builder(ViewList.this);
		if (type == R.drawable.ic_category) {
			String message = "";
			message += String.format(" %s 個目錄",
					categoryList.get(list_id).cat_count);
			message += String.format("\n %s 個文章",
					categoryList.get(list_id).count);

			ad.setTitle(categoryList.get(list_id).name);
			ad.setMessage(message);
			ad.setPositiveButton(android.R.string.ok, null);
			ad.setNegativeButton("進入", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					try {
						// 類型為目錄
						Log.i("List type",
								"目錄  => 編號:"
										+ categoryList.get(list_id).term_taxonomy_id);
						toNextPage(type, list_id, ViewList.class);
					} catch (Exception e) {
						Log.e(TAG, "ListView Category Click Error", e);
					}
				}
			});
		} else if (type == R.drawable.ic_post) {
			String message = "";
			message += String.format(" 發文者: %s",
					postList.get(list_id).post_author);
			message += String.format("\n 發佈時間: %s",
					postList.get(list_id).post_date);

			ad.setTitle(postList.get(list_id).post_title);
			ad.setMessage(message);
			ad.setPositiveButton(android.R.string.ok, null);
			ad.setNegativeButton("進入", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					try {
						// 類型為文章
						Log.i("List type", "文章  => 編號:"
								+ postList.get(list_id).id);
						toNextPage(type, list_id, ViewPost.class);
					} catch (Exception e) {
						Log.e(TAG, "ListView Post Click Error", e);
					}
				}
			});
		}
		ad.show();
		return true;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		// 按下時
		case MotionEvent.ACTION_DOWN:
			moveStatus = false;
			v.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.btn_click_t_bg));
			break;
		// 按下結束時
		case MotionEvent.ACTION_UP:
			v.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.btn_click_u_bg));
			break;
		}
		return super.onTouchEvent(event);
	}
}
