package tw.edu.hust.yu97024.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tw.edu.hust.yn97024.ApplocationBody;
import tw.edu.hust.yn97024.R;
import tw.edu.hust.yn97024.SettingActivity;
import tw.edu.hust.yn97024.packge.PostList;
import tw.edu.hust.yn97024.skill.SQLite;
import tw.edu.hust.yn97024.skill.Soap;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ViewPost extends Activity implements Runnable, OnClickListener, OnTouchListener {
	private final static String TAG = "ViewPost";
	private Intent intent;
	private int list_id;
	private Map<String, Object> post = new HashMap<String, Object>();
	private Soap soap = new Soap();
	private ProgressDialog progress;
	private final static int QUERY_POST_EMPTY_BACK = 0;
	private final static int QUERY_POST = 1;
	private final static int QUERY_NO_NETWORK = 2;
	private TextView text_title, text_footer, text_one, text_two, text_three;
	private ImageView ic_logo;
	private RelativeLayout btn_one, btn_two, btn_three;
	private WebView web_content;
	private SharedPreferences mPerferences;
	private SQLite sqlite;
	@SuppressWarnings("unused")
	private boolean moveStatus = false;
	private ApplocationBody applocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_post);

		// 偏好設定
		mPerferences = PreferenceManager.getDefaultSharedPreferences(this);
		// SQLite
		if(sqlite==null){
			applocation = (ApplocationBody) getApplication();
			sqlite = applocation.bodySQLite;
		}
		
		// 接收區
		intent = getIntent();

		List<PostList> regPostList;

		list_id = intent.getIntExtra("list_id", -1);
		if (list_id != -1) {
			// 取得目錄編號
			regPostList = intent.getParcelableArrayListExtra("postList");
			post.put("id", regPostList.get(list_id).id);
			post.put("post_author", regPostList.get(list_id).post_author);
			post.put("post_date", regPostList.get(list_id).post_date);
			post.put("post_title", regPostList.get(list_id).post_title);
			post.put("comment_count", regPostList.get(list_id).comment_count);
			post.put("cat_id", regPostList.get(list_id).cat_id);
		} else {
			Toast.makeText(this, "文章資料錯誤", 500).show();
			finish();
		}

		ic_logo = (ImageView) findViewById(R.id.ic_logo);
		text_title = (TextView) findViewById(R.id.text_title);
		text_footer = (TextView) findViewById(R.id.text_footer);
		text_one = (TextView) findViewById(R.id.text_one);
		text_two = (TextView) findViewById(R.id.text_two);
		text_three = (TextView) findViewById(R.id.text_three);
		btn_one = (RelativeLayout) findViewById(R.id.btn_one);
		btn_two = (RelativeLayout) findViewById(R.id.btn_two);
		btn_three = (RelativeLayout) findViewById(R.id.btn_three);
		web_content = (WebView) findViewById(R.id.web_content);

		text_title.setText((String) post.get("post_title"));
		text_footer.setText(String.format("由 %s 在 %s 發佈",
				post.get("post_author"), post.get("post_date")));

		text_one.setText("回上一頁");
		text_two.setText("設定");
		text_three.setText("關於");

		ic_logo.setTag("home");
		btn_one.setTag("back");
		btn_two.setTag("setting");
		btn_three.setTag("about");

		ic_logo.setOnClickListener(this);
		btn_one.setOnClickListener(this);
		btn_two.setOnClickListener(this);
		btn_three.setOnClickListener(this);
		btn_one.setOnTouchListener(this);
		btn_two.setOnTouchListener(this);
		btn_three.setOnTouchListener(this);

		// -- 取資料 -- //
		// 顯示載入視窗
		progress = ProgressDialog.show(ViewPost.this, "提示訊息", "資料載入中...",false, false);

		new Thread(this).start();
	}

	@Override
	public void run() {
		// 存放SOAP查回的資料
		String[][] soapQueryPost = null;
		try {
			if (mPerferences.getBoolean(applocation.KEY_USE_INTERNET_SQL_DATA, false)) {
				if(applocation.isNetwork()){
					soapQueryPost = soap.GetPostContent(String.valueOf(post.get("id")));
				} else {
					handler.obtainMessage(QUERY_NO_NETWORK , "您沒有開啟網路連線，請使用離線查看資料。").sendToTarget();
				}
			} else {
				String script = "";
				script = "SELECT post_content " +
						 " FROM post_content " +
						 " WHERE id = " + String.valueOf(post.get("id"));
				soapQueryPost = sqlite.SQLQuery(script);
			}
			
			if(soapQueryPost == null || soapQueryPost.length == 0){
				handler.obtainMessage(QUERY_POST_EMPTY_BACK, "還沒有取得此文章資料內容，請進入設定進行更新或使用網路瀏覽").sendToTarget();
			}
		} catch (Exception e) {
			Log.e(TAG, "SOAP get Post Data error!!", e);
		} finally {
			progress.dismiss();
			// 文章資料
			if (soapQueryPost != null && soapQueryPost.length > 0) {
				handler.obtainMessage(QUERY_POST, soapQueryPost).sendToTarget();
			}
		}
	}

	public Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			String[][] data = null;
			switch (msg.what) {
			case QUERY_POST_EMPTY_BACK:
				setResult(RESULT_OK);
				new AlertDialog.Builder(ViewPost.this)
				.setTitle("提示訊息")
				.setMessage((String)msg.obj)
				.setPositiveButton("確定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						ViewPost.this.finish();
					}
				})
				.setNegativeButton("設定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						intent = new Intent(ViewPost.this, SettingActivity.class);
						startActivityForResult(intent, RESULT_FIRST_USER);
						ViewPost.this.finish();
					}
				})
				.show();
				break;
			case QUERY_POST:
				data = (String[][]) msg.obj;
				web_content.loadDataWithBaseURL(null,
						"<pre><font color='#000000'>" + data[0][0]
								+ "</font></pre>", "text/html", "utf-8", null);

				web_content.setFocusableInTouchMode(true);
				web_content.getSettings().setSupportZoom(true);
				web_content.getSettings().setBuiltInZoomControls(true);
				web_content.getSettings().setJavaScriptEnabled(true);
				web_content.setBackgroundColor(Color.parseColor("#ffffff"));
				web_content.getSettings().setDefaultFontSize(10);
				
				// 插入SQLite
				if(mPerferences.getBoolean("useInternetSQLData", false)){
					String queryCount = "0";
					queryCount = sqlite
							.SQLQueryOne("SELECT COUNT(*) FROM post_content WHERE id = "
									+ post.get("id"));
					// SQLite有資料
					String script = "";
					if(Integer.valueOf(queryCount) > 0){
						script = "UPDATE post_content "+
								 " SET "+
								 " post_content = '" + ApplocationBody.prepare_string(data[0][0]) + "' " +
								 " WHERE id = '" + post.get("id") + "'; ";
						//Log.d("ViewList", script);
						sqlite.SQLExecute(script);
					} else {
						script = "INSERT INTO post_content (id, post_content) "+
						 " VALUES( " +
						 " '" + post.get("id") + "', " +
						 " '" + ApplocationBody.prepare_string(data[0][0]) + "' " +
						 " ); ";
						Log.d("ViewList", script);
						sqlite.SQLExecute(script);
					}
				}
				break;
			case QUERY_NO_NETWORK:
				new AlertDialog.Builder(ViewPost.this)
				.setTitle("提示訊息")
				.setMessage((String)msg.obj)
				.setPositiveButton("確定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						setResult(RESULT_CANCELED);
						ViewPost.this.finish();
					}
				})
				.setNegativeButton("設定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						intent = new Intent(ViewPost.this, SettingActivity.class);
						startActivityForResult(intent, RESULT_FIRST_USER);
					}
				})
				.show();
				break;
			}
		}
	};

	@Override
	public void onBackPressed() {
		findViewById(R.id.btn_one).setTag("back");
		onClick(findViewById(R.id.btn_one));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ic_logo:
		case R.id.btn_one:
		case R.id.btn_two:
		case R.id.btn_three:
			if (v.getTag().equals("back")) {
				setResult(RESULT_OK);
				finish();
			} else if (v.getTag().equals("setting")) {
				intent = new Intent(this, SettingActivity.class);
				startActivityForResult(intent,RESULT_FIRST_USER);
			} else if (v.getTag().equals("about")) {
				applocation.showAboutDialog(this);
			} else if (v.getTag().equals("home")) {
				setResult(RESULT_CANCELED);
				finish();
				intent = new Intent(this, ViewList.class);
				startActivity(intent);
			}
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case ApplocationBody.RESULT_SETTING:
			Log.d(TAG, "RESULT_SETTING");
			progress = ProgressDialog.show(this, "提示訊息", "資料載入中...",false, false);
			new Thread(this).start();
			break;
		case RESULT_CANCELED:
			setResult(RESULT_CANCELED);
			finish();
			break;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		// 按下時
		case MotionEvent.ACTION_DOWN:
			moveStatus = false;
			v.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.btn_click_t_bg));
			break;
		// 按下結束時
		case MotionEvent.ACTION_UP:
			v.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.btn_click_u_bg));
			break;
		}
		return super.onTouchEvent(event);
	}
}
