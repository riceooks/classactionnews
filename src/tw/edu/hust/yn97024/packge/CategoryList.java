package tw.edu.hust.yn97024.packge;

import android.os.Parcel;
import android.os.Parcelable;

public class CategoryList implements Parcelable{
	
	public String term_taxonomy_id = "";
	public String term_id = "";
	public String name = "";
	public String taxonomy = "";
	public String parent = "";
	public String count = "";
	public String cat_count = "";
	public String[] data;
	
	public CategoryList(String[] data){
		updateData(data);
		this.data = data;
	}
	
	public void updateData(String[] data){
		this.term_taxonomy_id = data[0];
		this.term_id = data[1];
		this.name = data[2];
		this.taxonomy = data[3];
		this.parent = data[4];
		this.count = data[5];
		this.cat_count = data[6];
	}

	@Override
	public int describeContents() {
		return hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(data);
	}
	
	public static final Parcelable.Creator<CategoryList> CREATOR
    		= new Parcelable.Creator<CategoryList>() {
		
		@Override
		public CategoryList createFromParcel(Parcel source) {
			
			String[] data = new String[7];
			source.readStringArray(data);
			CategoryList rval = new CategoryList(data);
			return rval;
		}

		@Override
		public CategoryList[] newArray(int size) {
			return new CategoryList[size];
		}
		
	};
}
