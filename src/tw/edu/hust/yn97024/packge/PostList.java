package tw.edu.hust.yn97024.packge;

import android.os.Parcel;
import android.os.Parcelable;

public class PostList implements Parcelable{
	
	public String id = "";
	public String post_author = "";
	public String post_date = "";
	public String post_title = "";
	public String comment_count = "";
	public String cat_id = "";
	public String[] data;
	
	public PostList(String[] data){
		updateData(data);
		this.data = data;
	}
	
	public void updateData(String[] data){
		this.id = data[0];
		this.post_author = data[1];
		this.post_date = data[2];
		this.post_title = data[3];
		this.comment_count = data[4];
		this.cat_id = data[5];
	}

	@Override
	public int describeContents() {
		return hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(data);
	}
	
	public static final Parcelable.Creator<PostList> CREATOR
    		= new Parcelable.Creator<PostList>() {
		
		@Override
		public PostList createFromParcel(Parcel source) {
			
			String[] data = new String[6];
			source.readStringArray(data);
			PostList rval = new PostList(data);
			return rval;
		}

		@Override
		public PostList[] newArray(int size) {
			return new PostList[size];
		}
		
	};
}
