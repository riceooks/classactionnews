package tw.edu.hust.yn97024.list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tw.edu.hust.yn97024.R;
import tw.edu.hust.yu97024.view.ViewList;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListCategory extends BaseAdapter {

	private List<Map<String, Object>> category = new ArrayList<Map<String, Object>>();
	ViewList mContext = null;
	static ViewHolder holder;
	
	/**
	 * 宣告元件<br>在這個適配器(Adapter)中需要用到的元件
	 * @author barry
	 *
	 */
	static class ViewHolder {
		ImageView ic_type,ic_arrow;
		TextView text_name;
	}
	
	/**
	 * 清除Adapter中的所有裝置
	 * @author barry
	 */
	public void clear(){
		category.clear();
	}
	
	/**
	 * 置入ListView圖片及裝置名稱
	 * @param ic_type 類型圖片
	 * @param text_name 列表文字
	 * @param ic_arrow 箭頭圖片
	 * @param type 類型<br>1=>目錄<br>2=>文章
	 * @author barry
	 */
	public void AppendItem(int ic_type, String text_name, Drawable ic_arrow, int list_id) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ic_type", ic_type);
		map.put("text_name", text_name);
		map.put("ic_arrow", ic_arrow);
		map.put("list_id", list_id);

		category.add(map);
	}

	/**
	 * 取得Context參考
	 * @param context
	 * @author barry
	 */
	public ListCategory(ViewList context) {
		mContext = context;
	}

	/**
	 * 取得裝置數量
	 * @author barry
	 */
	@Override
	public int getCount() {
		return category.size();
	}

	/**
	 * 取得裝置清單物件
	 * @author barry
	 */
	@Override
	public Object getItem(int position) {
		return position;
	}

	/**
	 * 取得裝置清單編號
	 * @author barry
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * 將資料置入Adapter
	 * @param position
	 * @param convertView
	 * @param parent
	 * @author barry
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(parent.getContext(), R.layout.list_category, null);
			holder = new ViewHolder();
			holder.ic_type = (ImageView) convertView.findViewById(R.id.ic_type);
			holder.text_name= (TextView) convertView.findViewById(R.id.text_name);
			holder.ic_arrow = (ImageView) convertView.findViewById(R.id.ic_arrow);
			
			// 樣式
			holder.text_name.setTextColor(Color.WHITE);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Map<String, Object> map = category.get(position);
		
		int rid = (Integer) map.get("ic_type");
		
		holder.ic_type.setImageDrawable(mContext.getResources().getDrawable(rid));
		holder.ic_type.setTag(rid);
		holder.text_name.setText((String) map.get("text_name"));
		holder.text_name.setTag((Integer) map.get("list_id"));
		holder.ic_arrow.setImageDrawable((Drawable)map.get("ic_arrow"));

		return convertView;
	}

}
