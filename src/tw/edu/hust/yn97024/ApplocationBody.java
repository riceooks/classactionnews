package tw.edu.hust.yn97024;

import java.io.IOException;
import java.io.InputStream;

import tw.edu.hust.yn97024.skill.SQLite;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.webkit.WebView;

public class ApplocationBody extends Application {
	public SQLite bodySQLite;
	public final String KEY_SETTING_ABOUT = "SETTING_ABOUT";
	public final String KEY_SETTING_HOME = "SETTING_HOME";
	public final String KEY_SETTING_BACK = "SETTING_BACK";
	public final String KEY_UPDATE_ACTION = "UPDATE_ACTION";
	public final String KEY_USE_INTERNET_SQL_DATA = "USE_INTERNET_SQL_DATA";
	public final String KEY_SETTING_INTERNET = "SETTING_INTERNET";
	
	public static final int RESULT_SETTING = 99;
	
	/** 關於用 */
	// 修平科技大學網址
	private final String KEY_ABOUT_HUST_WEBSITE = "http://www.hust.edu.tw/";
	// 修平科技大學-資訊網路技術系網址
	private final String KEY_ABOUT_HUSTINT_WEBSITE = "http://www.int.hust.edu.tw/";
	// 網站網址路徑
	private final String KEY_ABOUT_CLASS_WEBSITE = "http://hustint.soyenet.com/";
	// 本軟體Market安裝路徑
	private final String KEY_ABOUT_APP_WEBSITE = "https://market.android.com/details?id=tw.edu.hust.yn97024";
	// 指導老師網址
	private final String KEY_ABOUT_TEACHER_WEBSITE = "http://www.wkb.idv.tw/";
	// 開發者網址
	private final String KEY_ABOUT_DEVELOPER_WEBSITE = "http://barryblog.appsgoo.com/";
	private ConnectivityManager connManager;
	

	@Override
	public void onCreate() {
		super.onCreate();
		
		bodySQLite = new SQLite(this);
	}
	

	
	public static String prepare_string(String html){
		html = addSlashes(html);
		String[] search = {"\\", "'"};
		String replace = "";
		for(int i=0; i<search.length; i++){
			html = str_replace(search[i], replace, html);
		}
		return html;
	}
	
	private static String addSlashes(String str) {
		if (str == null)
			return "";

		StringBuffer s = new StringBuffer((String) str);
		for (int i = 0; i < s.length(); i++)
			if (s.charAt(i) == '\'')
				s.insert(i++, '\\');
		return s.toString();

	}
	
	private static String str_replace(String search, String replace, String subject) {
		StringBuffer result = new StringBuffer(subject);
		int pos = 0;
		while (true) {
			pos = result.indexOf(search, pos);
			if (pos != -1)
				result.replace(pos, pos + search.length(), replace);
			else
				break;
		}
		return result.toString();
	}
	
	public void showAboutDialog(Context context){
		// 設定網頁元件
		WebView webview = new WebView(context);
		webview.getSettings().setSupportZoom(false);
		webview.getSettings().setBuiltInZoomControls(false);
		webview.getSettings().setJavaScriptEnabled(false);
		
		// 讀取文字檔
		String textContent = "";
		try {
			InputStream is = getAssets().open("www/about.html");
			int size = is.available();

			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();

			textContent = new String(buffer, "UTF8");
			
			// 軟體版本
			String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			textContent = textContent.replaceAll("#version#", version);
			
			// 軟體名稱
			String appName = getResources().getString(R.string.app_name);
			textContent = textContent.replaceAll("#appName#", appName);
			
			// 學校網址
			textContent = textContent.replaceAll("#hustWebsite#", KEY_ABOUT_HUST_WEBSITE);
			
			// 科系網址
			textContent = textContent.replaceAll("#hustintWebsite#", KEY_ABOUT_HUSTINT_WEBSITE);
			
			// 網站網址
			textContent = textContent.replaceAll("#classWebsite#", KEY_ABOUT_CLASS_WEBSITE);
			
			// 軟體網址
			textContent = textContent.replaceAll("#appWebsite#", KEY_ABOUT_APP_WEBSITE);
			
			// 指導老師網址
			textContent = textContent.replaceAll("#teacherWebsite#", KEY_ABOUT_TEACHER_WEBSITE);
			
			// 開發者網址
			textContent = textContent.replaceAll("#developerWebsite#", KEY_ABOUT_DEVELOPER_WEBSITE);
			
			// 文字放入元件
			webview.loadData(textContent, "text/html", "utf-8");

		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		// 產生視窗
		new AlertDialog.Builder(context)
		.setIcon(getResources().getDrawable(R.drawable.ic_launcher))
		.setTitle("關於")
		.setView(webview)
		.setPositiveButton("確定", null)
		.show();
	}
	
	public boolean isNetwork(){
		connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connManager.getActiveNetworkInfo();
		if(info == null || !info.isConnected()){
			return false;
		} else {
			if(!info.isAvailable()){
				return false;
			} else {
				return true;
			}
		}
	}
}
