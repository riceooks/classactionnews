package tw.edu.hust.yn97024;

import java.util.Timer;
import java.util.TimerTask;

import tw.edu.hust.yu97024.view.ViewList;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

public class ClassActionNewsActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);
        
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		Timer nT1 = new Timer();
		nT1.schedule(new TimerTask() {
			public void run() {
				Intent intent = new Intent();
				intent.setClass(ClassActionNewsActivity.this, ViewList.class);
				startActivity(intent);
				ClassActionNewsActivity.this.finish();
			}
		}, 1000);
    }
}