package tw.edu.hust.yn97024;

import tw.edu.hust.yn97024.skill.SQLite;
import tw.edu.hust.yn97024.skill.Soap;
import tw.edu.hust.yu97024.view.ViewList;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingActivity extends PreferenceActivity implements
		OnClickListener, OnTouchListener, OnPreferenceChangeListener, OnPreferenceClickListener {
	private static final int UPDATE_END = 1;
	private ImageView ic_logo;
	private RelativeLayout btn_one, btn_two;
	private TextView text_one, text_two;
	private Intent intent;
	private SQLite sqlite;
	private Soap soap = new Soap();
	private CheckBoxPreference useInternetSQLData;
	private Preference updateAction,about,home,back,internet;
	private ProgressDialog progress;
	private ApplocationBody applocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.setting);
		setContentView(R.layout.view_setting);
		
		// SQLite
		if (sqlite == null) {
			applocation = (ApplocationBody) getApplication();
			sqlite = applocation.bodySQLite;
		}

		// 參考
		ic_logo = (ImageView) findViewById(R.id.ic_logo);
		btn_one = (RelativeLayout) findViewById(R.id.btn_one);
		btn_two = (RelativeLayout) findViewById(R.id.btn_two);
		text_one = (TextView) findViewById(R.id.text_one);
		text_two = (TextView) findViewById(R.id.text_two);
		about = (Preference) findPreference(applocation.KEY_SETTING_ABOUT);
		home = (Preference) findPreference(applocation.KEY_SETTING_HOME);
		back = (Preference) findPreference(applocation.KEY_SETTING_BACK);
		internet = (Preference) findPreference(applocation.KEY_SETTING_INTERNET);

		text_one.setText("回上一頁");
		text_two.setText("關於");

		ic_logo.setOnClickListener(this);
		btn_one.setOnClickListener(this);
		btn_two.setOnClickListener(this);
		btn_one.setOnTouchListener(this);
		btn_two.setOnTouchListener(this);
		about.setOnPreferenceClickListener(this);
		home.setOnPreferenceClickListener(this);
		back.setOnPreferenceClickListener(this);
		internet.setOnPreferenceClickListener(this);
		
		// 組態設定
		useInternetSQLData = (CheckBoxPreference)findPreference(applocation.KEY_USE_INTERNET_SQL_DATA);
		useInternetSQLData.setOnPreferenceChangeListener(this);
		
		// 全部更新
		updateAction = (Preference) findPreference(applocation.KEY_UPDATE_ACTION);
		if(useInternetSQLData.isChecked()){
			Log.d(this.getClass().getSimpleName(), "SQL is Checked");
			updateAction.setEnabled(false);
		} else {
			Log.d(this.getClass().getSimpleName(), "SQL no Checked");
			updateAction.setEnabled(true);
		}
		updateAction.setOnPreferenceClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ic_logo:
			setResult(RESULT_CANCELED);
			finish();
			intent = new Intent(this, ViewList.class);
			startActivity(intent);
			break;
		case R.id.btn_one:
			onBackPressed();
			break;
		case R.id.btn_two:
			applocation.showAboutDialog(this);
			break;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		// 按下時
		case MotionEvent.ACTION_DOWN:
			v.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.btn_click_t_bg));
			break;
		// 按下結束時
		case MotionEvent.ACTION_UP:
			v.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.btn_click_u_bg));
			break;
		}
		return super.onTouchEvent(event);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		final CheckBoxPreference pk_useInternetSQLData = (CheckBoxPreference)preference;
		if (preference.getKey().equals(applocation.KEY_USE_INTERNET_SQL_DATA)
				&& (Boolean) newValue) {

			new AlertDialog.Builder(this)
			.setTitle("提示訊息")
			.setMessage("當取消勾選這個項目時，會使用已經瀏覽過或更新過的資料，建議使用全部更新過後再取消勾選。")
			.setPositiveButton("取消", 
					new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Log.d(this.getClass().getSimpleName(), "Click Cancel");
							pk_useInternetSQLData.setChecked(false);
							updateAction.setEnabled(true);
						}
					})
			.setNegativeButton("確定",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							Log.d(this.getClass().getSimpleName(), "Click OK");
							pk_useInternetSQLData.setChecked(true);
						}
					}).show();
			updateAction.setEnabled(false);
		} else {
			Log.d(this.getClass().getSimpleName(), "Click Cancel");
			pk_useInternetSQLData.setChecked(false);
			updateAction.setEnabled(true);
		}
		return true;
	}
	
	/**
	 * 回上一頁
	 */
	@Override
	public void onBackPressed() {
		setResult(ApplocationBody.RESULT_SETTING);
		finish();
	}

	/**
	 * Preference 點擊事件
	 * @param preference
	 */
	@Override
	public boolean onPreferenceClick(Preference preference) {
		// 網路設定
		if(preference.getKey().equals(applocation.KEY_SETTING_INTERNET)){
			startActivityForResult(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS), RESULT_FIRST_USER);
		}
		// 關於
		else if(preference.getKey().equals(applocation.KEY_SETTING_ABOUT)){
			applocation.showAboutDialog(this);
		}
		// 回首頁
		else if(preference.getKey().equals(applocation.KEY_SETTING_HOME)){
			onClick(findViewById(R.id.ic_logo));
		}
		// 回上一頁
		else if(preference.getKey().equals(applocation.KEY_SETTING_BACK)){
			onBackPressed();
		}
		// 更新資料
		else if(preference.getKey().equals(applocation.KEY_UPDATE_ACTION)){
			if(applocation.isNetwork()){
				new AlertDialog.Builder(this)
				.setTitle("更新資料")
				.setMessage("如果現在更新，需要一些時間，待更新完畢時可以離線查看資料，您確定嗎？")
				.setPositiveButton("取消", null)
				.setNegativeButton("確定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						progress = new ProgressDialog(SettingActivity.this);
						progress.setTitle("資料更新中");
						progress.setMessage("當資料更新完畢時，會自動關閉程式，只需按下通知列即可重新開始執行程式。");
						progress.setCancelable(false);
						progress.show();
						
						new Thread(){
							public void run(){
								String[][] tableData = null;
								try {
									
									// 下載 term_taxonomy
									tableData = soap.Table_term_taxonomy_Query();
									if(tableData != null && tableData.length > 0){
										// 清空資料表
										sqlite.SQLExecute("DELETE FROM term_taxonomy; "+
														  " SELECT * FROM sqlite_sequence; " +
														  " UPDATE sqlite_sequence SET seq=0 " +
														  " WHERE name='term_taxonomy'");
										String script = "";
										
										for(int i=0; i<tableData.length; i++){
											script = "INSERT INTO term_taxonomy (term_taxonomy_id, term_id, taxonomy, description, parent, count) " +
											 		 " VALUES ( ";
											for(int j=0; j<tableData[i].length; j++){
												if(j!=0){
													script += ",";
												}
												script += " '" + tableData[i][j] + "' ";
											}
											script += ")";
											sqlite.SQLExecute(script);
											Log.d(this.getClass().getSimpleName(), script);
										}
									}
									// 下載terms
									tableData = soap.Table_terms_Query();
									if(tableData != null && tableData.length > 0){
										// 清空資料表
										sqlite.SQLExecute("DELETE FROM terms; "+
														  " SELECT * FROM sqlite_sequence; " +
														  " UPDATE sqlite_sequence SET seq=0 " +
														  " WHERE name='terms'");
										String script = "";
										
										for(int i=0; i<tableData.length; i++){
											script = "INSERT INTO terms (term_id, name, slug, term_group) " +
											 		 " VALUES ( ";
											for(int j=0; j<tableData[i].length; j++){
												if(j!=0){
													script += ",";
												}
												script += " '" + tableData[i][j] + "' ";
											}
											script += ")";
											sqlite.SQLExecute(script);
											Log.d(this.getClass().getSimpleName(), script);
										}
									}
									// 下載users
									tableData = soap.Table_users_Query();
									if(tableData != null && tableData.length > 0){
										// 清空資料表
										sqlite.SQLExecute("DELETE FROM users; "+
														  " SELECT * FROM sqlite_sequence; " +
														  " UPDATE sqlite_sequence SET seq=0 " +
														  " WHERE name='users'");
										String script = "";
										
										for(int i=0; i<tableData.length; i++){
											script = "INSERT INTO users (ID,user_login,user_pass,user_nicename,user_email,user_url,user_registered,user_activation_key,user_status,display_name) " +
											 		 " VALUES ( ";
											for(int j=0; j<tableData[i].length; j++){
												if(j!=0){
													script += ",";
												}
												script += " '" + tableData[i][j] + "' ";
											}
											script += ")";
											sqlite.SQLExecute(script);
											Log.d(this.getClass().getSimpleName(), script);
										}
									}
									// 下載term_relationships
									tableData = soap.Table_term_relationships_Query();
									if(tableData != null && tableData.length > 0){
										// 清空資料表
										sqlite.SQLExecute("DELETE FROM term_relationships; "+
														  " SELECT * FROM sqlite_sequence; " +
														  " UPDATE sqlite_sequence SET seq=0 " +
														  " WHERE name='term_relationships'");
										String script = "";
										
										for(int i=0; i<tableData.length; i++){
											script = "INSERT INTO term_relationships (object_id,term_taxonomy_id,term_order) " +
											 		 " VALUES ( ";
											for(int j=0; j<tableData[i].length; j++){
												if(j!=0){
													script += ",";
												}
												script += " '" + tableData[i][j] + "' ";
											}
											script += ")";
											sqlite.SQLExecute(script);
											Log.d(this.getClass().getSimpleName(), script);
										}
									}
									// 下載posts
									tableData = soap.Table_posts_Query();
									if(tableData != null && tableData.length > 0){
										// 清空資料表
										sqlite.SQLExecute("DELETE FROM posts; "+
														  " SELECT * FROM sqlite_sequence; " +
														  " UPDATE sqlite_sequence SET seq=0 " +
														  " WHERE name='posts'");
										String script = "";
										
										for(int i=0; i<tableData.length; i++){
											script = "INSERT INTO posts (ID,post_author,post_date,post_date_gmt,post_content,post_title,post_excerpt,post_status,comment_status,ping_status,post_password,post_name,to_ping,pinged,post_modified,post_modified_gmt,post_content_filtered,post_parent,guid,menu_order,post_type,post_mime_type,comment_count) " +
											 		 " VALUES ( ";
											for(int j=0; j<tableData[i].length; j++){
												if(j!=0){
													script += ",";
												}
												script += " '" + ApplocationBody.prepare_string(tableData[i][j]) + "' ";
											}
											script += ")";
											sqlite.SQLExecute(script);
											Log.d(this.getClass().getSimpleName(), script);
										}
									}
									
									//*/
									
									// 處理所有資料
									// 清空資料表
									sqlite.SQLExecute("DELETE FROM category_list; "+
													  " SELECT * FROM sqlite_sequence; " +
													  " UPDATE sqlite_sequence SET seq=0 " +
													  " WHERE name='category_list'");
									// 取得所有目錄(只有兩階層)
									String getID = "0";
									String script = "";
									tableData = getCategoryList(getID);
									String regID = "";
									// 第一層結構
									if(tableData != null && tableData.length > 0){
										for(int i=0; i<tableData.length; i++){
											script = "INSERT INTO category_list (term_taxonomy_id,term_id,name,taxonomy,parent,count,cat_count) " +
											 		 " VALUES ( ";
											for(int j=0; j<tableData[i].length; j++){
												if(j!=0){
													script += ",";
												}
												script += " '" + ApplocationBody.prepare_string(tableData[i][j]) + "' ";
											}
											script += ")";
											sqlite.SQLExecute(script);
											Log.d(this.getClass().getSimpleName(), script);
											if(Integer.valueOf(tableData[i][6]) > 0){
												regID += tableData[i][0]+",";
											}
										}
									}
									
									String[] regIDchg = regID.substring(0, regID.length()-1).split(",");
									Log.d(this.getClass().getSimpleName(), "regIDchg = " + regID);
									
									
									// 第二層結構
									for(int k=0; k<regIDchg.length; k++){
										tableData = getCategoryList(regIDchg[k]);
										if(tableData != null && tableData.length > 0){
											for(int i=0; i<tableData.length; i++){
												script = "INSERT INTO category_list (term_taxonomy_id,term_id,name,taxonomy,parent,count,cat_count) " +
												 		 " VALUES ( ";
												for(int j=0; j<tableData[i].length; j++){
													if(j!=0){
														script += ",";
													}
													script += " '" + ApplocationBody.prepare_string(tableData[i][j]) + "' ";
												}
												script += ")";
												sqlite.SQLExecute(script);
												Log.d(this.getClass().getSimpleName(), script);
											}
										}
									}
									
									// 取得所有文章
									// 清空資料表
									sqlite.SQLExecute("DELETE FROM posts_list; "+
													  " SELECT * FROM sqlite_sequence; " +
													  " UPDATE sqlite_sequence SET seq=0 " +
													  " WHERE name='posts_list'");
									sqlite.SQLExecute("DELETE FROM post_content; "+
													  " SELECT * FROM sqlite_sequence; " +
													  " UPDATE sqlite_sequence SET seq=0 " +
													  " WHERE name='post_content'");
									
									String[][] categoryListID = sqlite.SQLQuery("SELECT term_taxonomy_id FROM category_list ORDER BY term_taxonomy_id");
									
									String[][] postsData = null;
									String postDataID = "";
									String[][] postsContent = null;
									for(int i=0; i<categoryListID.length; i++){
										postsData = getPostsList(categoryListID[i][0]);
										Log.d(this.getClass().getSimpleName(), "目錄編號: " + categoryListID[i][0]);
										if(postsData != null && postsData.length > 0){
											for(int j=0; j<postsData.length; j++){
												// 文章列表
												script = "INSERT INTO posts_list (id,post_author,post_date,post_title,comment_count,cat_id) " +
												 		 " VALUES ( ";
												for(int k=0; k<postsData[j].length; k++){
													if(k!=0){
														script += ",";
													}
													script += " '" + postsData[j][k] + "' ";
												}
												script += ")";
												sqlite.SQLExecute(script);
												
												Log.d(this.getClass().getSimpleName(), "文章編號--: " + postsData[j][0]);
												
												postDataID += postsData[j][0] + ",";
											}
										}
									}
									
									String[] regDataID = postDataID.substring(0, postDataID.length()-1).split(",");
									for(String id:regDataID){
										// 文章內容
										postsContent = getPostsContent(id);
										
										if(postsContent != null && postsContent.length > 0){
											script = "INSERT INTO post_content (id,post_content) " +
											 		 " VALUES ( ";
											script += " '" + postsContent[0][0] + "', " +
													  " '" + ApplocationBody.prepare_string(postsContent[0][1]) + "' ";
											script += ")";
											sqlite.SQLExecute(script);
											Log.d(this.getClass().getSimpleName(), "文章內容: " + script);
											//*/
										}
									}
									
									
								} catch (Exception e) {
									
								} finally {
									progress.dismiss();
									handler.obtainMessage(UPDATE_END, "").sendToTarget();
								}
							}
						}.start();
					}
				})
				.show();
			} else {
				new AlertDialog.Builder(this)
				.setTitle("提示訊息")
				.setMessage("您沒有開啟網路連線，請檢查網路設定。")
				.setPositiveButton("確定", null)
				.show();
			}
		}
		
		return true;
	}
	
	public Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){
				case UPDATE_END:
				callNotify("資料更新完成，請點選使用");
				break;
			}
		}
	};
	
	/**
	 * 通知欄工作<br>讓訊息出現在手機上方通知欄
	 * @param text 要顯示的內容訊息
	 */
	public void callNotify(String text) {
		// 取得Notification服務
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// 設定當按下這個通知之後要執行的activity
		Intent notifyIntent = new Intent(this, ClassActionNewsActivity.class);
		notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent appIntent = PendingIntent.getActivity(this, 0, notifyIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		Notification notification = new Notification();
		// 設定出現在狀態列的圖示
		notification.icon = R.drawable.ic_launcher;
		// 顯示在狀態列的文字
		notification.tickerText = text;
		// 會有通知預設的鈴聲、振動、light
		notification.defaults = Notification.DEFAULT_ALL;
		// 設定通知的標題、內容
		notification.setLatestEventInfo(this, getResources().getString(R.string.app_name), text, appIntent);
		// 點擊狀態後消失
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		
		// 送出Notification
		notificationManager.notify(0, notification);
		
		// 結束
		finish();
	}
	
	
	
	/**
	 * 取得旗下目錄
	 * @param parent 父目錄編號
	 * @return
	 */
	private String[][] getCategoryList(String parent){
		String[][] data = null;
		try {
			data = sqlite.SQLQuery("SELECT tt.term_taxonomy_id, t.term_id, t.name, tt.taxonomy, tt.parent, tt.count, " +
											  " (SELECT COUNT(*) FROM term_taxonomy AS tt1 WHERE tt1.parent = tt.term_id) AS cat_count " +
											  " FROM terms AS t, term_taxonomy AS tt " +
											  " WHERE tt.term_id = t.term_id AND tt.taxonomy = 'category' AND parent = " + parent +
											  " AND (tt.count > 0 OR (SELECT COUNT( * ) FROM term_taxonomy AS tt1 WHERE tt1.parent = tt.term_id) > 0) " +
											  " ORDER BY t.term_id ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
	/**
	 * 取得文章列表
	 * @param cat_id 目錄編號
	 * @return
	 */
	private String[][] getPostsList(String cat_id){
		String[][] data = null;
		try {
			data = sqlite.SQLQuery("SELECT p.ID, " +
								   " (SELECT display_name FROM users AS u WHERE u.ID = p.post_author) AS post_author, " +
								   " p.post_date, p.post_title, p.comment_count, (SELECT " + cat_id + ") AS cat_id " +
								   " FROM term_relationships AS tr, posts AS p " +
								   " WHERE tr.object_id = p.ID " +
								   " AND tr.term_taxonomy_id = " + cat_id +
								   " AND p.post_status = 'publish' " +
								   " ORDER BY p.ID DESC ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
	/**
	 * 取得文章內容
	 * @param post_id 文章編號
	 * @return
	 */
	private String[][] getPostsContent(String post_id){
		String[][] data = null;
		try {
			data = sqlite.SQLQuery("SELECT id,post_content  " +
								   " FROM posts " +
								   " WHERE ID = " + post_id +
								   " LIMIT 1 ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
}
