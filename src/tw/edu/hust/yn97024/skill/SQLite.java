package tw.edu.hust.yn97024.skill;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLite extends SQLiteOpenHelper{
	//private static final String DATABASE_NAME = "/mnt/sdcard/hustint/ClassActionNews.db";
	private static final String DATABASE_NAME = "hustint";
	private static final int DATABASE_VERSION = 2;
	private SQLiteDatabase db;
	
	/* 與網站同步資料表區域 */
	// <!--文章資料表-->//
	private static final String TABLE_CREATE_posts =
		"CREATE TABLE IF NOT EXISTS " + "posts" + " ( " +
		" id INTEGER unsigned, " +
		" post_author INTEGER unsigned NOT NULL default '0'," +
		" post_date DATETIME NOT NULL default '0000-00-00 00:00:00', " +
		" post_date_gmt DATETIME NOT NULL default '0000-00-00 00:00:00', " +
		" post_content INTEGER unsigned, " +
		" post_title TEXT NOT NULL, " +
		" post_excerpt TEXT NOT NULL, " +
		" post_status VARCHAR(20) NOT NULL default 'publish', " +
		" comment_status VARCHAR(20) NOT NULL default 'open', " +
		" ping_status VARCHAR(20) NOT NULL default 'open', " +
		" post_password VARCHAR(20) NOT NULL default '', " +
		" post_name VARCHAR(200) NOT NULL default '', " +
		" to_ping TEXT NOT NULL, " +
		" pinged TEXT NOT NULL, " +
		" post_modified DATETIME NOT NULL default '0000-00-00 00:00:00', " +
		" post_modified_gmt DATETIME NOT NULL default '0000-00-00 00:00:00', " +
		" post_content_filtered TEXT NOT NULL, " +
		" post_parent INTEGER unsigned NOT NULL default '0', " +
		" guid VARCHAR(255) NOT NULL default '', " +
		" menu_order INT NOT NULL default '0', " +
		" post_type VARCHAR(255) NOT NULL default 'post', " +
		" post_mime_type VARCHAR(100) NOT NULL default '', " +
		" comment_count INTEGER NOT NULL default '0', " +
		" PRIMARY KEY (id)" + ");";
	
	// <!--目錄資料表-->//
	private static final String TABLE_CREATE_terms = 
		"CREATE TABLE IF NOT EXISTS " + "terms" + " ( " +
		" term_id INTEGER, " +
		" name VARCHAR(200)," +
		" slug VARCHAR(200), " +
		" term_group INTEGER, " +
		" PRIMARY KEY (term_id)" + ");";
	
	// <!--目錄文章關係資料表-->//
	private static final String TABLE_CREATE_term_relationships = 
		"CREATE TABLE IF NOT EXISTS " + "term_relationships" + " ( " +
		" object_id INTEGER, " +
		" term_taxonomy_id INTEGER," +
		" term_order INTEGER, " +
		" PRIMARY KEY (object_id,term_taxonomy_id)" + ");";
	
	// <!--目錄對應資料表-->//
	private static final String TABLE_CREATE_term_taxonomy = 
		"CREATE TABLE IF NOT EXISTS " + "term_taxonomy" + " ( " +
		" term_taxonomy_id INTEGER, " +
		" term_id INTEGER," +
		" taxonomy VARCHAR(32), " +
		" description TEXT, " +
		" parent INTEGER," +
		" count INTEGER," +
		" PRIMARY KEY (term_taxonomy_id)" + ");";
	
	// <!--會員資料表-->//
	private static final String TABLE_CREATE_users =
		"CREATE TABLE IF NOT EXISTS " + "users" + " ( " +
		" ID INTEGER , " +
		" user_login VARCHAR(60), " +
		" user_pass VARCHAR(64), " +
		" user_nicename VARCHAR(50), " +
		" user_email VARCHAR(100), " +
		" user_url VARCHAR(100), " +
		" user_registered VARCHAR(20) DEFAULT '0000-00-00 00:00:00', " +
		" user_activation_key VARCHAR(60), " +
		" user_status INTEGER, " +
		" display_name VARCHAR(250), " +
		" PRIMARY KEY (ID)" + ");";
	
	
	/* 行動專用資料庫區域 */
	// <!--目錄列表--> //
	private static final String TABLE_CREATE_category_list = 
		"CREATE TABLE IF NOT EXISTS " + "category_list" + " ( " +
		" term_taxonomy_id INTEGER, " +
		" term_id INTEGER," +
		" name VARCHAR(200), " +
		" taxonomy VARCHAR(32), " +
		" parent INTEGER," +
		" count INTEGER," +
		" cat_count INTEGER," +
		" PRIMARY KEY (term_taxonomy_id)" + ");";
	
	// <!--文章列表--> //
	private static final String TABLE_CREATE_posts_list = 
		"CREATE TABLE IF NOT EXISTS " + "posts_list" + " ( " +
		" id INTEGER, " +
		" post_author INTEGER," +
		" post_date DATETIME, " +
		" post_title TEXT, " +
		" comment_count INTEGER," +
		" cat_id INTEGER, " +
		" PRIMARY KEY (id)" + ");";
	
	// <!--文章內容--> //
	private static final String TABLE_CREATE_post_content = 
		"CREATE TABLE IF NOT EXISTS " + "post_content" + " ( " +
		" id INTEGER, " +
		" post_content LONGTEXT, " +
		" PRIMARY KEY (id)" + ");";
	
	//context=內容物件；name=傳入資料庫名稱；factory=複雜查詢時使用；version=資料庫版本
	public SQLite(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		db = this.getWritableDatabase();
	}
	
	// ***** 物件生成時查無此資料庫名稱，建立SQLITE資料表(Start) *****//
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_CREATE_posts);
		db.execSQL(TABLE_CREATE_terms);
		db.execSQL(TABLE_CREATE_term_relationships);
		db.execSQL(TABLE_CREATE_term_taxonomy);
		db.execSQL(TABLE_CREATE_users);

		db.execSQL(TABLE_CREATE_category_list);
		db.execSQL(TABLE_CREATE_posts_list);
		db.execSQL(TABLE_CREATE_post_content);
	}
	
	// ***** SQLITE資料庫版本變更時執行(Start) *****//
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//db.execSQL("DROP TABLE IF EXISTS config");
	}

	// ***** 查詢資料表資料 *****//
	public String[][] SQLQuery(String sql){
		Cursor cursor = db.rawQuery(sql, null);
		int columnCount = cursor.getColumnCount();
		int rowCount = 0;
		String tableData[][] = new String[cursor.getCount()][cursor.getColumnCount()];
		while(cursor.moveToNext()){	
			for(int i=0; i<columnCount; i++){
				tableData[rowCount][i] = cursor.getString(i);
			}
			rowCount++;
		}
		cursor.close();
		return tableData;		
	}
	
	// ***** 查詢資料表單一資料 ***** //
	public String SQLQueryOne(String sql){
		Cursor cursor = db.rawQuery(sql, null);
		int columnCount = cursor.getColumnCount();
		int rowCount = 0;
		String tableData[][] = new String[cursor.getCount()][cursor.getColumnCount()];
		while(cursor.moveToNext()){	
			for(int i=0; i<columnCount; i++){
				tableData[rowCount][i] = cursor.getString(i);
			}
			rowCount++;
		}
		cursor.close();
		return tableData[0][0];
	}

	// ***** 新增資料表資料(Start) *****//
	public void SQLExecute(String sql){
		try{
			db.execSQL(sql); 
		} catch (Exception e) {
			Log.e("SQLite", "SQL Execute Error", e);
		}
	}
	// ***** 新增資料表資料(End) *****//
}
