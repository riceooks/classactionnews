package tw.edu.hust.yn97024.skill;

import java.io.IOException;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;

public class Soap {
	// ***** SOAP2 固定使用的網域及網址(Start) *****//
	String NAMESPACE = "http://hustint.soyenet.com/";
	String URL = "http://hustint.soyenet.com/wp-soap/class.php";
	String METHOD_NAME;
	String SOAP_ACTION = "http://hustint.soyenet.com/wp-soap/class.php/";
	SoapObject detail;
	SoapObject soap;
	SoapSerializationEnvelope envelope;
	HttpTransportSE ht;
	private int timeOut = 8000;

	/**
	 * 以父分類編號取得旗下分類
	 */
	public String[][] GetCategory(String parent) {
		METHOD_NAME = "term_taxonomy_Query";
		SOAP_ACTION = SOAP_ACTION + METHOD_NAME;

		String[][] query = null;
		soap = new SoapObject(NAMESPACE, METHOD_NAME);

		// <!--方法傳入參數-->//
		soap.addProperty("parent", Integer.valueOf(parent));	//父分類編號

		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.bodyOut = soap;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(soap);
		
		// 設定TimeOut時間

		ht = new HttpTransportSE(URL, timeOut);
		ht.debug = false;

		try {
			ht.call(SOAP_ACTION, envelope);
			Vector<?> vector = (Vector<?>) envelope.getResponse();

			query = new String[vector.size()][];
			for (int i = 0; i < vector.size(); i++) {
				detail = (SoapObject) vector.get(i);

				// <!--取得的資料欄位-->//
				String[] fieldName = { "term_taxonomy_id",
									   "term_id",
									   "name", 
									   "taxonomy", 
									   "parent", 
									   "count", 
									   "cat_count" };

				query[i] = new String[Integer.valueOf(fieldName.length)];
				for (int j = 0; j < Integer.valueOf(fieldName.length); j++) {
					query[i][j] = String.valueOf(detail
							.getProperty(fieldName[j]));
				}
			}

		} catch (IOException e) {
			// <!--當資料寫入發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error IOException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "IO";
		} catch (XmlPullParserException e) {
			// <!--當XML發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error XmlPullParserException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "XmlPullParser";
		} catch (Exception e) {
			// <!--其它例外錯誤-->//
			query = new String[1][2];
			Log.e("Soap ErrorException ", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "Other";
		}

		return query;
	}
	
	/**
	 * 以分類目錄編號取得文章列表
	 */
	public String[][] GetPostList(String cat_id) {
		METHOD_NAME = "postsList_Query";
		SOAP_ACTION = SOAP_ACTION + METHOD_NAME;

		String[][] query = null;
		soap = new SoapObject(NAMESPACE, METHOD_NAME);

		// <!--方法傳入參數-->//
		soap.addProperty("cat_id", Integer.valueOf(cat_id));	//分類編號

		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.bodyOut = soap;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(soap);

		ht = new HttpTransportSE(URL);
		ht.debug = false;

		try {
			ht.call(SOAP_ACTION, envelope);
			Vector<?> vector = (Vector<?>) envelope.getResponse();

			query = new String[vector.size()][];
			for (int i = 0; i < vector.size(); i++) {
				detail = (SoapObject) vector.get(i);

				// <!--取得的資料欄位-->//
				String[] fieldName = { "id",
									   "post_author",
									   "post_date", 
									   "post_title",
									   "comment_count",
									   "cat_id"};

				query[i] = new String[Integer.valueOf(fieldName.length)];
				for (int j = 0; j < Integer.valueOf(fieldName.length); j++) {
					query[i][j] = String.valueOf(detail
							.getProperty(fieldName[j]));
				}
			}

		} catch (IOException e) {
			// <!--當資料寫入發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error IOException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "IO";
		} catch (XmlPullParserException e) {
			// <!--當XML發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error XmlPullParserException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "XmlPullParser";
		} catch (Exception e) {
			// <!--其它例外錯誤-->//
			query = new String[1][2];
			Log.e("Soap ErrorException ", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "Other";
		}

		return query;
	}
	
	
	/**
	 * 以文章編號取得文章內容
	 */
	public String[][] GetPostContent(String id) {
		METHOD_NAME = "postsContent_Query";
		SOAP_ACTION = SOAP_ACTION + METHOD_NAME;

		String[][] query = null;
		soap = new SoapObject(NAMESPACE, METHOD_NAME);

		// <!--方法傳入參數-->//
		soap.addProperty("ID", Integer.valueOf(id));	//文章編號

		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.bodyOut = soap;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(soap);

		ht = new HttpTransportSE(URL);
		ht.debug = false;

		try {
			ht.call(SOAP_ACTION, envelope);
			Vector<?> vector = (Vector<?>) envelope.getResponse();

			query = new String[vector.size()][];
			for (int i = 0; i < vector.size(); i++) {
				detail = (SoapObject) vector.get(i);

				// <!--取得的資料欄位-->//
				String[] fieldName = { "post_content" };

				query[i] = new String[Integer.valueOf(fieldName.length)];
				for (int j = 0; j < Integer.valueOf(fieldName.length); j++) {
					query[i][j] = String.valueOf(detail
							.getProperty(fieldName[j]));
				}
			}

		} catch (IOException e) {
			// <!--當資料寫入發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error IOException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "IO";
		} catch (XmlPullParserException e) {
			// <!--當XML發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error XmlPullParserException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "XmlPullParser";
		} catch (Exception e) {
			// <!--其它例外錯誤-->//
			query = new String[1][2];
			Log.e("Soap ErrorException ", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "Other";
		}

		return query;
	}
	
	/**
	 * 取得資料表posts的資料
	 * @return
	 * 二維陣列<br>
	 * 每筆資料為一陣列<br>
	 * 每筆資料包含內容為
	 * "ID","post_author","post_date","post_date_gmt","post_content","post_title","post_excerpt","post_status","comment_status","ping_status","post_password","post_name","to_ping","pinged","post_modified","post_modified_gmt","post_content_filtered","post_parent","guid","menu_order","post_type","post_mime_type","comment_count"
	 */
	public String[][] Table_posts_Query(){
		METHOD_NAME = "table_posts_Query";
		SOAP_ACTION = SOAP_ACTION + METHOD_NAME;

		String[][] query = null;
		soap = new SoapObject(NAMESPACE, METHOD_NAME);

		// <!--方法傳入參數-->//
		//soap.addProperty("ID", Integer.valueOf(id));	//文章編號

		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.bodyOut = soap;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(soap);

		ht = new HttpTransportSE(URL);
		ht.debug = false;

		try {
			ht.call(SOAP_ACTION, envelope);
			Vector<?> vector = (Vector<?>) envelope.getResponse();

			query = new String[vector.size()][];
			for (int i = 0; i < vector.size(); i++) {
				detail = (SoapObject) vector.get(i);

				// <!--取得的資料欄位-->//
				String[] fieldName = { "post_id",
										"post_author",
										"post_date",
										"post_date_gmt",
										"post_content",
										"post_title",
										"post_excerpt",
										"post_status",
										"comment_status",
										"ping_status",
										"post_password",
										"post_name",
										"to_ping",
										"pinged",
										"post_modified",
										"post_modified_gmt",
										"post_content_filtered",
										"post_parent",
										"guid",
										"menu_order",
										"post_type",
										"post_mime_type",
										"comment_count"};

				query[i] = new String[Integer.valueOf(fieldName.length)];
				for (int j = 0; j < Integer.valueOf(fieldName.length); j++) {
					query[i][j] = String.valueOf(detail
							.getProperty(fieldName[j]));
				}
			}

		} catch (IOException e) {
			// <!--當資料寫入發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error IOException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "IO";
		} catch (XmlPullParserException e) {
			// <!--當XML發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error XmlPullParserException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "XmlPullParser";
		} catch (Exception e) {
			// <!--其它例外錯誤-->//
			query = new String[1][2];
			Log.e("Soap ErrorException ", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "Other";
		}

		return query;
	}
	
	/**
	 * 取得資料表term_relationships的資料
	 * @return
	 * 二維陣列<br>
	 * 每筆資料為一陣列<br>
	 * 每筆資料包含內容為
	 *  "object_id","term_taxonomy_id","term_order"
	 */
	public String[][] Table_term_relationships_Query(){
		METHOD_NAME = "table_term_relationships_Query";
		SOAP_ACTION = SOAP_ACTION + METHOD_NAME;

		String[][] query = null;
		soap = new SoapObject(NAMESPACE, METHOD_NAME);

		// <!--方法傳入參數-->//
		//soap.addProperty("ID", Integer.valueOf(id));	//文章編號

		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.bodyOut = soap;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(soap);

		ht = new HttpTransportSE(URL);
		ht.debug = false;

		try {
			ht.call(SOAP_ACTION, envelope);
			Vector<?> vector = (Vector<?>) envelope.getResponse();

			query = new String[vector.size()][];
			for (int i = 0; i < vector.size(); i++) {
				detail = (SoapObject) vector.get(i);

				// <!--取得的資料欄位-->//
				String[] fieldName = { "object_id",
										"term_taxonomy_id",
										"term_order" };

				query[i] = new String[Integer.valueOf(fieldName.length)];
				for (int j = 0; j < Integer.valueOf(fieldName.length); j++) {
					query[i][j] = String.valueOf(detail
							.getProperty(fieldName[j]));
				}
			}

		} catch (IOException e) {
			// <!--當資料寫入發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error IOException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "IO";
		} catch (XmlPullParserException e) {
			// <!--當XML發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error XmlPullParserException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "XmlPullParser";
		} catch (Exception e) {
			// <!--其它例外錯誤-->//
			query = new String[1][2];
			Log.e("Soap ErrorException ", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "Other";
		}

		return query;
	}
	
	/**
	 * 取得資料表users的資料
	 * @return
	 * 二維陣列<br>
	 * 每筆資料為一陣列<br>
	 * 每筆資料包含內容為
	 *  "ID","user_login","user_pass","user_nicename","user_email","user_url","user_registered","user_activation_key","user_status","display_name"
	 */
	public String[][] Table_users_Query(){
		METHOD_NAME = "table_users_Query";
		SOAP_ACTION = SOAP_ACTION + METHOD_NAME;

		String[][] query = null;
		soap = new SoapObject(NAMESPACE, METHOD_NAME);

		// <!--方法傳入參數-->//
		//soap.addProperty("ID", Integer.valueOf(id));	//文章編號

		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.bodyOut = soap;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(soap);

		ht = new HttpTransportSE(URL);
		ht.debug = false;

		try {
			ht.call(SOAP_ACTION, envelope);
			Vector<?> vector = (Vector<?>) envelope.getResponse();

			query = new String[vector.size()][];
			for (int i = 0; i < vector.size(); i++) {
				detail = (SoapObject) vector.get(i);

				// <!--取得的資料欄位-->//
				String[] fieldName = { "user_id",
										"user_login",
										"user_pass",
										"user_nicename",
										"user_email",
										"user_url",
										"user_registered",
										"user_activation_key",
										"user_status",
										"display_name" };

				query[i] = new String[Integer.valueOf(fieldName.length)];
				for (int j = 0; j < Integer.valueOf(fieldName.length); j++) {
					query[i][j] = String.valueOf(detail
							.getProperty(fieldName[j]));
				}
			}

		} catch (IOException e) {
			// <!--當資料寫入發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error IOException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "IO";
		} catch (XmlPullParserException e) {
			// <!--當XML發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error XmlPullParserException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "XmlPullParser";
		} catch (Exception e) {
			// <!--其它例外錯誤-->//
			query = new String[1][2];
			Log.e("Soap ErrorException ", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "Other";
		}

		return query;
	}
	
	/**
	 * 取得資料表terms的資料
	 * @return
	 * 二維陣列<br>
	 * 每筆資料為一陣列<br>
	 * 每筆資料包含內容為
	 *  "term_id","name", "slug","term_group"
	 */
	public String[][] Table_terms_Query(){
		METHOD_NAME = "table_terms_Query";
		SOAP_ACTION = SOAP_ACTION + METHOD_NAME;

		String[][] query = null;
		soap = new SoapObject(NAMESPACE, METHOD_NAME);

		// <!--方法傳入參數-->//
		//soap.addProperty("ID", Integer.valueOf(id));	//文章編號

		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.bodyOut = soap;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(soap);

		ht = new HttpTransportSE(URL);
		ht.debug = false;

		try {
			ht.call(SOAP_ACTION, envelope);
			Vector<?> vector = (Vector<?>) envelope.getResponse();

			query = new String[vector.size()][];
			for (int i = 0; i < vector.size(); i++) {
				detail = (SoapObject) vector.get(i);

				// <!--取得的資料欄位-->//
				String[] fieldName = { "term_id",
									   "name",
									   "slug",
									   "term_group" };

				query[i] = new String[Integer.valueOf(fieldName.length)];
				for (int j = 0; j < Integer.valueOf(fieldName.length); j++) {
					query[i][j] = String.valueOf(detail
							.getProperty(fieldName[j]));
				}
			}

		} catch (IOException e) {
			// <!--當資料寫入發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error IOException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "IO";
		} catch (XmlPullParserException e) {
			// <!--當XML發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error XmlPullParserException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "XmlPullParser";
		} catch (Exception e) {
			// <!--其它例外錯誤-->//
			query = new String[1][2];
			Log.e("Soap ErrorException ", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "Other";
		}

		return query;
	}
	
	/**
	 * 取得資料表term_taxonomy的資料
	 * @return
	 * 二維陣列<br>
	 * 每筆資料為一陣列<br>
	 * 每筆資料包含內容為
	 *  "term_taxonomy_id","term_id","taxonomy","description","parent","count"
	 */
	public String[][] Table_term_taxonomy_Query(){
		METHOD_NAME = "table_term_taxonomy_Query";
		SOAP_ACTION = SOAP_ACTION + METHOD_NAME;

		String[][] query = null;
		soap = new SoapObject(NAMESPACE, METHOD_NAME);

		// <!--方法傳入參數-->//
		//soap.addProperty("ID", Integer.valueOf(id));	//文章編號

		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.bodyOut = soap;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(soap);

		ht = new HttpTransportSE(URL);
		ht.debug = false;

		try {
			ht.call(SOAP_ACTION, envelope);
			Vector<?> vector = (Vector<?>) envelope.getResponse();

			query = new String[vector.size()][];
			for (int i = 0; i < vector.size(); i++) {
				detail = (SoapObject) vector.get(i);

				// <!--取得的資料欄位-->//
				String[] fieldName = { "term_taxonomy_id",
									   "term_id",
									   "taxonomy",
									   "description",
									   "parent",
									   "count"};

				query[i] = new String[Integer.valueOf(fieldName.length)];
				for (int j = 0; j < Integer.valueOf(fieldName.length); j++) {
					query[i][j] = String.valueOf(detail
							.getProperty(fieldName[j]));
				}
			}

		} catch (IOException e) {
			// <!--當資料寫入發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error IOException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "IO";
		} catch (XmlPullParserException e) {
			// <!--當XML發生錯誤-->//
			query = new String[1][2];
			Log.e("Soap Error XmlPullParserException", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "XmlPullParser";
		} catch (Exception e) {
			// <!--其它例外錯誤-->//
			query = new String[1][2];
			Log.e("Soap ErrorException ", "" + e.getMessage());
			query[0][0] = "Exception";
			query[0][1] = "Other";
		}

		return query;
	}
}
